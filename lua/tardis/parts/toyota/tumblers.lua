local PART={}
PART.ID = "toyota_tumblers"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/tumblers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.SoundOn = "Poogie/toyota/controls/tumblers_on.wav"
PART.SoundOff = "Poogie/toyota/controls/tumblers_off.wav"

TARDIS:AddPart(PART)