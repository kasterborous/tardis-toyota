local PART={}
PART.ID = "toyota_gears"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/gears.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.7
PART.Sound = "cem/toyota/gears.wav"

TARDIS:AddPart(PART)