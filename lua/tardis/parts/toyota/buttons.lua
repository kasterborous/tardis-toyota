local PART={}
PART.ID = "toyota_buttons"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/buttons.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.SoundOn = "Poogie/toyota/controls/buttons_on.wav"
PART.SoundOff = "cem/toyota/buttons.wav"

TARDIS:AddPart(PART)