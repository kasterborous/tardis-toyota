local PART={}
PART.ID = "toyota_spin12"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/spin12.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "cem/toyota/crank2.wav"

TARDIS:AddPart(PART)