local PART={}
PART.ID = "toyota_key"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/key.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "Poogie/toyota/controls/key.wav"

TARDIS:AddPart(PART)