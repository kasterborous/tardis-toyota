local PART={}
PART.ID = "toyota_cranks"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/cranks.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "cem/toyota/crank.wav"

TARDIS:AddPart(PART)