local PART={}
PART.ID = "toyota_button2"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/button.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 6
PART.Sound = "cem/toyota/buttons.wav"

TARDIS:AddPart(PART)