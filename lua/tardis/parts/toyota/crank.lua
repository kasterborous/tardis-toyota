local PART={}
PART.ID = "toyota_crank"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/crank.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.8
PART.SoundOff = "cem/toyota/crank.wav"
PART.SoundOn = "Poogie/toyota/controls/crank1.wav"

TARDIS:AddPart(PART)

