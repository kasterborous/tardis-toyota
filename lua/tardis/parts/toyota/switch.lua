local PART={}
PART.ID = "toyota_switch"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.SoundOn = "Poogie/toyota/others/switch.wav"
PART.SoundOff = "cem/toyota/switch.wav"

TARDIS:AddPart(PART)

